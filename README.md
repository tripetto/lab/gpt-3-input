# Tripetto GPT-3 Experiment Input
This folder contains a set of example forms for Tripetto. In the Tripetto world, forms are stored as a JSON structure called the form definition. The goal of the GPT-3 experiment would be to generate a JSON form definition using AI.

The form definitions in this repository are the ones shown on this page: https://tripetto.com/examples/

Besides those examples are two simple forms that serve as a good starting point to understand the data structure of Tripetto:
- `simple-contact-form.json`: A simple 3 field contact form without any fancy logic;
- `simple-logic-example.json`: A simple logic example with a Yes/No question and a separate branch for yes or no.

## Running a form
We have created 3 simple Codepen templates to make it easy to test and run a form definition in one of our stock runners. Just click one of the links below and copy-paste a form definition JSON on line #9 in the Codepen code editor. The form should start running right away.

- Autoscroll runner: https://codepen.io/tripetto/pen/a84e10bd4b69619b64a43201b437cfb8
- Chat runner: https://codepen.io/tripetto/pen/2d3f258832a5158d2b846442e6ad8233
- Classic runner: https://codepen.io/tripetto/pen/cbd3d103f75cc1d67cfcca5cf1e8e64f

## Viewing a form in the builder
To view a form definition in the form builder we created another Codepen template. Click the link below and again copy-paste the form definition JSON, this time on line #10.

https://codepen.io/tripetto/pen/7746d217f74bb627561850311d4beaad

## JSON structure
When you dive into the JSON structure of a form, you will find the following types of data elements:

### Nodes
These are the containers for the actual form building blocks (i.e. question types like text input, dropdown, checkbox etc.). A node is basically a placeholder for a block. The node behavior itself is defined in a block.

### Clusters
One or more nodes can be placed in a so-called cluster. It is simply a collection of nodes.

### Branches
One or more clusters can form a branch. A branch can be conditional. You can define certain conditions for the branch to be taken or skipped.

### Conditions
A branch can contain one or more conditions, which are used to direct flow into the branch. They are evaluated when a cluster ends. Only subsequent branches with matching condition(s) are taken by the runner. Just like nodes, the conditions are actually placeholders for condition behaviors. The condition behavior itself is defined in a condition block.

### Blocks
So, blocks supply a certain behavior to nodes and conditions that are used in the builder to create smart forms. As mentioned before, blocks come in two flavors:

- **Node blocks**: Provide building blocks for nodes (for example, text input, dropdown, checkbox, etc.);
- **Condition blocks**: Provide building blocks for conditions (for example, evaluate a certain given answer).

### Slots
All data collected through the runner needs to be stored somewhere. In Tripetto we use `Slots` to do this. `Slots` are also defined by blocks. For example, if you build a text-input block, you're probably going to retrieve a text value from an input control in your runner implementation. In that case, your block should create a slot to store this text value. There are different types of slots, such as a `String`, `Boolean` or `Number`.

### Basic structure
Each form definition begins with a root branch. This root branch holds the first clusters array that holds the nodes and branches for each cluster.

- Root branch
  - Name of the form
  - Language of the form (ISO code)
  - Clusters array
    - Nodes array
      - Slots array
    - Branches array
      - Conditions array
      - Clusters array

The clusters array is recursive. It allows nesting branches and their conditions to create logic in forms. If you look closely at the form definition examples, you can identify this recursive pattern.

## Identifiers in the form structure
Each item in the form structure needs to have a unique id within the form. Those ids are lowercased SHA256 hashes (you can generate them here: https://passwordsgenerator.net/sha256-hash-generator/). We need to investigate if GPT-3 can generate those hashes. If not, just omit them in the AI process (leave all `"id"` fields empty). We can generate them later using a preproduction script.
